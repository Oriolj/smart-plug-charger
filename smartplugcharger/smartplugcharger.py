import time

import click
import pywemo
import subprocess as sp
import http.client
import urllib

DEBUG = os.getenv('DEBUG', False)

TIME_BEFORE_CHARGE = 3600 * 3
OFF_WHEN_PERCENTAGE_DROP = 0.10
POLLING_INTERVAL = 10

# TODO: support tp-link hs110
# TODO: timeout send push, right now if it fails it hangs the script
# TODO: put aprox cost on report, depending on cost per kwh, report also kwh spent
# TODO: the logic to start inmediatly fails (from button)
# TODO: leave it running and detect when wemo is turned on
#       manually as a trigger to start the charging


def _clear_screen() -> None:
    if DEBUG:
        return
    sp.call('clear', shell=True)


def _send_pushover_notification(po_app_token: str, po_user_key: str, msg: str):
    conn = http.client.HTTPSConnection("api.pushover.net:443")
    conn.request("POST", "/1/messages.json",
        urllib.parse.urlencode({
            "token": po_app_token,
            "user": po_user_key,
            "message": msg,
        }), {"Content-Type": "application/x-www-form-urlencoded"})
    print(conn.getresponse().status)


def run_watcher(address: str, po_app_token: str, po_user_key: str) -> None:
    port = pywemo.ouimeaux_device.probe_wemo(address)
    if port is None:
        .....foobar
    url = 'http://%s:%i/setup.xml' % (address, port)
    device = pywemo.discovery.device_from_description(url, None)

    _clear_screen()

    last_state = device.get_state()  # 0 off, 1 on, 8 on but load is off
    time.sleep(POLLING_INTERVAL)
    charge_requested = False
    charge_started_ts = -1
    charge_requested_ts = -1

    max_watts_read = 0
    last_watts_readings = [-1, -1, -1, -1, -1, -1]

    #print(device)
    #print(last_state)
    #print(device.current_power)

    _send_pushover_notification(po_app_token, po_user_key, "Smart charger ready")

    while True:
        current_state = device.get_state(force_update=True)
        if current_state == 8:
            current_state = 1
        if current_state == 1 and last_state == 0 and not charge_requested:
            device.toggle()
            charge_requested = True
            charge_requested_ts = time.time()
            current_state = 0
            msg = f"Charge scheduled, starting on {TIME_BEFORE_CHARGE} seconds"
            print(msg)
            _send_pushover_notification(po_app_token, po_user_key, msg)
        elif charge_requested and ((time.time()-charge_requested_ts) < TIME_BEFORE_CHARGE) and current_state == 0:
            _clear_screen()
            print(f"Waiting {int(TIME_BEFORE_CHARGE - (time.time()-charge_requested_ts))} seconds to start charge")
            #_send_pushover_notification(po_app_token, po_user_key, msg)
        elif charge_requested and current_state == 0:
            print("Starting charge...")
            _send_pushover_notification(po_app_token, po_user_key, "Starting charge...")
            device.toggle()
            last_state = current_state = 1

        elif current_state == 0 and not charge_requested:
            _clear_screen()
            print("Waiting for start command...")

        if current_state == 1:
            last_state = current_state = 1
            if charge_started_ts == -1:
                charge_started_ts = time.time()
                print("Starting charge...")
                _send_pushover_notification(po_app_token, po_user_key, "Starting charge...")
            charge_requested =  False
            charge_requested_ts = -1

            current_watts = (device.current_power // 1000)
            print(f"Current watts: {current_watts}w")
            if current_watts > max_watts_read:
                max_watts_read = current_watts
            
            last_watts_readings.pop(0)
            last_watts_readings.append((device.current_power // 1000))
            finish_charging = False
            if last_watts_readings[-1] == 0 and last_watts_readings[-2] == 0 and last_watts_readings[-3] == 0:
                # we read 0 watts for three consecutive readings
                finish_charging = True
            elif last_watts_readings[-1] < max_watts_read and last_watts_readings[-2] < max_watts_read and last_watts_readings[-3] < max_watts_read:
                # Watts are dropping for three consecutive readings
                if last_watts_readings[-1] == -1 or last_watts_readings[-2] == -1 or last_watts_readings[-3] == -1:
                    pass
                else:
                    if ((max_watts_read / current_watts)-1) > OFF_WHEN_PERCENTAGE_DROP:
                        finish_charging = True

            if finish_charging:
                hours_to_charge = round((time.time()-charge_started_ts)/3600, 2)
                msg = f"Charge finished, turning off charger, max watts: {max_watts_read}w, charging time: {hours_to_charge}h"
                print(msg)
                _send_pushover_notification(po_app_token, po_user_key, msg)
                device.toggle()
                last_state = 0
                max_watts_read = 0
                last_watts_readings = [-1, -1, -1, -1, -1, -1]
                charge_started_ts = -1
                if DEBUG:
                    return

        time.sleep(POLLING_INTERVAL)


@click.command()
@click.option('--ip', prompt='IP')
@click.option('--po_app_token', prompt='Pushover app token')
@click.option('--po_user_key', prompt='Pushover user key')
def main(ip: str, po_app_token: str, po_user_key: str):
    run_watcher(ip, po_app_token, po_user_key)


if __name__ == '__main__':
    main()
