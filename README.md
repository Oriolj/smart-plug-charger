# Smart plug charger
# TODO: instructions to restart always with system?

For now it only supports belkin wemo insight  

Running it with docker:  
$ iptables -t nat -A POSTROUTING ! -o docker0 -s 172.17.0.0/16 -j MASQUERADE  

$ docker pull oriolj/smart-plug-charger  
$ docker run --restart="always" --net=host python /smartplugcharger.py --ip "<ip>" --po_user_key "<pushover user key>"  --po_app_token "<pushover app token>" oriolj/smart-plug-charger  
