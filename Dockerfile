FROM python:3.7-alpine

WORKDIR /

RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev linux-headers

RUN pip install pipenv

COPY Pipfile .
COPY Pipfile.lock .

RUN pipenv install --system --deploy
RUN rm Pipfile
RUN rm Pipfile.lock

COPY smartplugcharger/smartplugcharger.py .
